module Cardgame {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001;
    exports edu.ntnu.idatt2001;
}