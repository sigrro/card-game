package edu.ntnu.idatt2001;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.util.ArrayList;

public class GUI extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        //set stage
        primaryStage.setTitle("Card game");
        primaryStage.setWidth(720);
        primaryStage.setHeight(540);

        //set scrollpane
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setFitToHeight(true);
        scrollPane.setFitToWidth(true);

        //set borderpane
        BorderPane borderPane = new BorderPane();
        borderPane.setMaxWidth(720);
        borderPane.setMaxHeight(540);

        //borderpane is displayed in scrollpane
        scrollPane.setContent(borderPane);

        //text for flush
        Text t = new Text("Flush:");
        t.setTranslateX(100);
        t.setTranslateY(230);
        t.setFill(Color.web("#e8692a"));
        t.setStyle("-fx-font-size: 16px;");

        TextArea textArea = new TextArea();
        textArea.setTranslateX(50);
        textArea.setTranslateY(-50);
        textArea.setMaxWidth(410);
        textArea.setMaxHeight(200);
        textArea.setEditable(false);
        textArea.setStyle("-fx-background-color: #735CDD; -fx-control-inner-background: #B3C2F2; " +
                "-fx-text-fill: #7612ff;  -fx-font-size: 16px;");

        TextArea smallTextArea = new TextArea();
        smallTextArea.setTranslateX(160);
        smallTextArea.setTranslateY(200);
        smallTextArea.setMaxWidth(100);
        smallTextArea.setMaxHeight(10);
        smallTextArea.setEditable(false);
        smallTextArea.setStyle("-fx-background-color: #9000B3; -fx-control-inner-background: #7E007B; " +
                "-fx-text-fill: #e8692a; -fx-font-size: 16px;");

        //buttons-----------------------------------------------------------------

        DeckOfCards deck = new DeckOfCards();
        HandOfCards hand = new HandOfCards(deck.dealHand(5));

        Button button = new Button("Deal hand");
        button.setTranslateX(500);
        button.setTranslateY(100);
        button.setOnAction(e -> {
            smallTextArea.setText("");
            textArea.setText(hand.getHandWithCards(deck.dealHand(5)).toString());
            if (deck.getDeckSize() < 5) {
                textArea.setText("No more cards in deck, reset the deck");
                button.setDisable(true);
            }
        });
        button.setStyle("-fx-text-fill: #fc0380; -fx-background-color: #ffc2e1; -fx-border-color: #fc0380;");

        Button button2 = new Button("Check hand");
        button2.setTranslateX(500);
        button2.setTranslateY(130);
        button2.setOnAction(e -> {
            if (deck.getDeckSize() < 5) {
                textArea.setText("You have to reset the deck before you can check the hand");
            } else {
                smallTextArea.setText(hand.isFlush());
            }
        });
        button2.setStyle("-fx-text-fill: #0056bd; -fx-background-color: #90b8e8; -fx-border-color: #0056bd;");

        Button button3 = new Button("Reset deck");
        button3.setTranslateX(500);
        button3.setTranslateY(160);
        button3.setOnAction(e -> {
            deck.fillDeckWithCards();
            textArea.setText("");
            smallTextArea.setText("");
            button.setDisable(false);
        });
        button3.setStyle("-fx-text-fill: #11660b; -fx-background-color: #9ee899; -fx-border-color: #11660b;");
        //------------------------------------------------------------------------

        //new vbox
        VBox vBox = new VBox();

        //add buttons and text to vbox
        vBox.getChildren().addAll(button, button2, button3, t, smallTextArea, textArea);

        //add stackpane to scrollpane
        scrollPane.setContent(vBox);
        borderPane.setCenter(scrollPane);

        //set scene
        Scene scene = new Scene(vBox, 720, 540);
        scene.setFill(Color.web("#fc0380"));
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}


