package edu.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
        private static DeckOfCards deckOfCards;
        private ArrayList<PlayingCard> cardslist;

        /**
         * Before running all other test methods, it runs this method for initializing
         * creates a new deckOfCards
         */
        @BeforeEach
        void setUp(){
            deckOfCards = new DeckOfCards();
            this.cardslist = new ArrayList<>();
        }

        @Test
        @DisplayName("Test getDeckSize method")
        public void testGetDeckSize() {
            assertEquals(52, deckOfCards.getDeckSize());
        }

        @Test
        @DisplayName("Test that checks correct amount of cards in deck")
        public void getCards_listWithCardsGiven_ExpectedCorrectSize(){
            assertEquals(52, deckOfCards.getCardsList().size());
        }

        @Test
        @DisplayName("Test that checks if correct number of cards is dealt")
        public void dealHand_positiveNumberGiven_ShouldDealCorrectNumberOfCards(){
            ArrayList<PlayingCard> handOfCards = (ArrayList<PlayingCard>) deckOfCards.dealHand(5);
            assertEquals(5, handOfCards.size());
        }

        @Test
        @DisplayName("Test that checks if exception is thrown if argument is <0")
        public void dealHand_NegativeNumbergiven_ExceptionThrown(){
            assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(-1));
        }
}