package edu.ntnu.idatt2001;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {
    private char suit;
    private int face;

    @BeforeEach
    void setUp() {
        this.suit = suit;
        this.face = face;
    }

    @Nested
    @DisplayName("Positive test cases")
    public class PositiveTestCases {
        @Test
        @DisplayName("Constructor for new cards")
        public void constructorNewCardsTest(){
            PlayingCard playingCard = new PlayingCard('S', 11);
            assertEquals('S'+ "" + 11, playingCard.toString());
        }

        @Test
        @DisplayName("Test playingcard as string")
        public void testPlayingCardAsString() {
            PlayingCard playingCard = new PlayingCard('S', 11);
            assertEquals('S'+ "" + 11, playingCard.getAsString());
        }

        @Test
        @DisplayName("Test getFace method")
        public void testGetFaceMethod() {
            PlayingCard playingCard = new PlayingCard('S', 11);
            assertEquals(11, playingCard.getFace());
        }

        @Test
        @DisplayName("Test getSuit method")
        public void testGetSuitMethod() {
            PlayingCard playingCard = new PlayingCard('S', 11);
            assertEquals('S', playingCard.getSuit());
        }

        @Test
        @DisplayName("Test getToString method")
        public void testGetToStringMethod() {
            PlayingCard playingCard = new PlayingCard('S', 11);
            assertEquals('S'+ "" + 11, playingCard.toString());
        }
    }

    @Nested
    @DisplayName("Negative test cases")
    public class NegativeTestCases {
        @Test
        @DisplayName("Check if throws when suit is blank")
        public void checkIfThrowsWhenSuitIsBlank() {
            assertThrows(IllegalArgumentException.class, () -> new PlayingCard(' ', 11));
        }
    }
}